import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'antd/dist/antd.css';
import './index.css';
import firebase from 'firebase';

  var firebaseConfig = {
    apiKey: "AIzaSyA9TxxghVqHU2Lg8QWZ1qi-7eAGQTxa9Cw",
    authDomain: "react-3b028.firebaseapp.com",
    projectId: "react-3b028",
    storageBucket: "react-3b028.appspot.com",
    messagingSenderId: "447557739267",
    appId: "1:447557739267:web:3b81514bb112ad9718ff36",
    measurementId: "G-V2M1W01PCB"
  };
    // Initialize Firebase
  firebase.initializeApp(firebaseConfig);


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
